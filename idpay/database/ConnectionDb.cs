﻿using System;
using idpay.model.domain;
using Microsoft.EntityFrameworkCore;

namespace idpay.database
{
	public class ConnectionDb : DbContext
	{
        public ConnectionDb(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<DataDemografi> DataDemografi { get; set; }
        public DbSet<DataPhotoID> DataPhotoID { get; set; }
    }
}

