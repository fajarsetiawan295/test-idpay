﻿using System;
using AutoMapper;
using idpay.Authorization;
using idpay.database;
using idpay.model.domain;
using idpay.model.request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.PixelFormats;

namespace idpay.Controllers;

[Authorization.Authorize]
[ApiController]
[Route("[controller]")]
public class DataDemografiController : ControllerBase
{

    private readonly ILogger<DataDemografiController> _logger;
    private readonly ConnectionDb _dbContext;
    private readonly IJwtUtils _jwtUtils;

    public DataDemografiController(ILogger<DataDemografiController> logger, ConnectionDb dbContext, IJwtUtils jwtUtils)
    {
        _logger = logger;
        _dbContext = dbContext;
        _jwtUtils = jwtUtils;
    }

    [HttpPost("")]
    [Authorization.Authorize]
    public IActionResult CreateData(DataDemografiRequest model)
    {
        if (_dbContext.DataDemografi.Any(x => x.Nik == model.Nik))
            return NotFound(new { message = "nik already exsist" });


        DataDemografi data = new DataDemografi();
        data.Nik = model.Nik;
        data.Nama = model.Nama;
        data.Tempat_lahir = model.Tempat_lahir;
        data.Tanggal_lahir = model.Tanggal_lahir;
        data.Jenis_kelamin = model.Jenis_kelamin;
        data.Golongan_darah = model.Golongan_darah;
        data.Alamat = model.Alamat;
        data.Rt = model.Rt;
        data.Rw = model.Rw;
        data.Kelurahan = model.Kelurahan;
        data.Desa = model.Desa;
        data.Kecamatan = model.Kecamatan;
        data.Kota = model.Kota;
        data.Provinsi = model.Provinsi;
        data.Agama = model.Agama;
        data.Kode_pos = model.Kode_pos;
        data.Status_perkawinan = model.Status_perkawinan;
        data.Pekerjaan = model.Pekerjaan;
        data.Kewarganegaraan = model.Kewarganegaraan;
        data.Masa_berlaku = model.Masa_berlaku;
        // save
        _dbContext.DataDemografi.Add(data);
        _dbContext.SaveChanges();

        var imagePath  = "images/" + model.Nik + ".png";

        DataPhotoID dataPhoto = new DataPhotoID();
        dataPhoto.nik = model.Nik;
        dataPhoto.Path_photo = imagePath;
        _dbContext.DataPhotoID.Add(dataPhoto);
        _dbContext.SaveChanges();

        ConvertBase64ToImage(model.Path_photo, imagePath);

        return Ok(new { message = "save successful" });
    }

    [HttpPut("{id}")]
    [Authorization.Authorize]
    public IActionResult UpdateData(int id, DataDemografiRequest model)
    {
        var data = _dbContext.DataDemografi.Find(id);
        if (data == null) return NotFound(new { message = "User not found" });

        if (model.Nik != data.Nik && _dbContext.DataDemografi.Any(x => x.Nik == model.Nik))
            return NotFound(new { message = "Username '" + model.Nik + "' is already taken" });

            
        if(model.Path_photo == "" || model.Path_photo == null)
            return NotFound(new { message = "photo required" });

        data.Nik = model.Nik;
        data.Nama = model.Nama;
        data.Tempat_lahir = model.Tempat_lahir;
        data.Tanggal_lahir = model.Tanggal_lahir;
        data.Jenis_kelamin = model.Jenis_kelamin;
        data.Golongan_darah = model.Golongan_darah;
        data.Alamat = model.Alamat;
        data.Rt = model.Rt;
        data.Rw = model.Rw;
        data.Kelurahan = model.Kelurahan;
        data.Desa = model.Desa;
        data.Kecamatan = model.Kecamatan;
        data.Kota = model.Kota;
        data.Provinsi = model.Provinsi;
        data.Agama = model.Agama;
        data.Kode_pos = model.Kode_pos;
        data.Status_perkawinan = model.Status_perkawinan;
        data.Pekerjaan = model.Pekerjaan;
        data.Kewarganegaraan = model.Kewarganegaraan;
        data.Masa_berlaku = model.Masa_berlaku;
        // save
        _dbContext.DataDemografi.Update(data);
        _dbContext.SaveChanges();

        if (model.Path_photo != "" || model.Path_photo != null)
        {
            var dataPhoto = _dbContext.DataPhotoID.Where(e => e.nik == model.Nik).First();

            var imagePath = "images/" + model.Nik + ".png";

            dataPhoto.nik = model.Nik;
            dataPhoto.Path_photo = imagePath;
            _dbContext.DataPhotoID.Update(dataPhoto);
            _dbContext.SaveChanges();

            ConvertBase64ToImage(model.Path_photo, imagePath);
        }
      

        return Ok(new { message = "save successful" });

    }
    [HttpGet("")]
    [Authorization.Authorize]
    public IActionResult GetData()
    {
        return Ok(_dbContext.DataDemografi.ToList());
    }

    [HttpGet("{id}")]
    [Authorization.Authorize]
    public IActionResult GetDetail(int id)
    {
        return Ok(_dbContext.DataDemografi.Find(id));
    }

    [HttpDelete("{id}")]
    [Authorization.Authorize]
    public IActionResult DeleteData(int id)
    {
        var data = _dbContext.DataDemografi.Find(id);
        _dbContext.DataDemografi.Remove(data);
        _dbContext.SaveChanges();

        return Ok(new { message = "success" });
    }

    public static void ConvertBase64ToImage(string base64String, string savePath)
    {
        var base64Data = base64String.Split(',')[1];

        var imageBytes = Convert.FromBase64String(base64Data);

        using (var image = Image.Load(imageBytes))
        {
            // Save the image to the specified path with auto-detected format
            image.Save(savePath);
        }
    }
}

