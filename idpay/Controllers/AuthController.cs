﻿using System;
using AutoMapper;
using idpay.Authorization;
using idpay.database;
using idpay.model.domain;
using idpay.model.request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace idpay.Controllers;

[Authorization.Authorize]
[ApiController]
[Route("[controller]")]
public class AuthController : ControllerBase
{
  
    private readonly ILogger<AuthController> _logger;
    private readonly ConnectionDb _dbContext;
    private readonly IJwtUtils _jwtUtils;




    public AuthController(ILogger<AuthController> logger, ConnectionDb dbContext, IJwtUtils jwtUtils)
    {
        _logger = logger;
        _dbContext = dbContext;
        _jwtUtils = jwtUtils;
    }

    [HttpPost("register")]
    [Authorization.AllowAnonymous]
    public IActionResult Register(RegisterRequest model)
    {
        if (_dbContext.Users.Any(x => x.Username == model.Username))
            return NotFound(new { message = "username already exsist" });


        Users user = new Users();
        user.Username = model.Username;

        // hash password
        user.Password = BCrypt.Net.BCrypt.HashPassword(model.Password);

        // save user
        _dbContext.Users.Add(user);
        _dbContext.SaveChanges();

        return Ok(new { message = "Registration successful" });
    }

    [HttpPost("login")]
    [Authorization.AllowAnonymous]
    public IActionResult Login(RegisterRequest model)
    {
        var user = _dbContext.Users.SingleOrDefault(x => x.Username == model.Username);
        if (user == null)
            return NotFound(new { message = "username not found" });

        if (user == null || !BCrypt.Net.BCrypt.Verify(model.Password, user.Password))
            return NotFound(new { message = "password is incorrect" });


        AuthRespone response = new AuthRespone();
        response.Username = user.Username;
        response.Id = user.Id;
        response.Token = _jwtUtils.GenerateToken(user);
        return Ok(new { response });

    }

}

