﻿using System;
using System.ComponentModel.DataAnnotations;

namespace idpay.model.request
{
	public class DataDemografiRequest
	{
       

        [Required]
        public string Nik { get; set; }
        [Required]
        public string Nama { get; set; }
        [Required]
        public string Tempat_lahir { get; set; }
        [Required]
        public DateTime Tanggal_lahir { get; set; }
        [Required]
        public string Jenis_kelamin { get; set; }
        [Required]
        public string Golongan_darah { get; set; }
        [Required]
        public string Alamat { get; set; }
        [Required]
        public string Rt { get; set; }
        [Required]
        public string Rw { get; set; }
        [Required]
        public string Kelurahan { get; set; }
        [Required]
        public string Desa { get; set; }
        [Required]
        public string Kecamatan { get; set; }
        [Required]
        public string Kota { get; set; }
        [Required]
        public string Provinsi { get; set; }
        [Required]
        public string Agama { get; set; }
        [Required]
        public string Kode_pos { get; set; }
        [Required]
        public string Status_perkawinan { get; set; }
        [Required]
        public string Pekerjaan { get; set; }
        [Required]
        public string Kewarganegaraan { get; set; }
        [Required]
        public DateTime Masa_berlaku { get; set; }

        public string Path_photo { get; set; }


    }
}

