﻿using System;
namespace idpay.model.request
{
    public class AuthRespone
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }
}

