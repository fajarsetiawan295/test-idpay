﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace idpay.model.domain
{
    [Table("data_photo_id")]
    public class DataPhotoID
	{
            public int Id { get; set; }
            public string nik { get; set; }
            public string Path_photo { get; set; }
    }
}

