﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace idpay.model.domain
{
    [Table("data_demografi")]
    public class DataDemografi
	{
        
        public int Id { get; set; }
        public string Nik { get; set; }
        public string Nama { get; set; }
        public string Tempat_lahir { get; set; }
        public DateTime Tanggal_lahir { get; set; }
        public string Jenis_kelamin { get; set; }
        public string Golongan_darah { get; set; }
        public string Alamat { get; set; }
        public string Rt { get; set; }
        public string Rw { get; set; }
        public string Kelurahan { get; set; }
        public string Desa { get; set; }
        public string Kecamatan { get; set; }
        public string Kota { get; set; }
        public string Provinsi { get; set; }
        public string Agama { get; set; }
        public string Kode_pos { get; set; }
        public string Status_perkawinan { get; set; }
        public string Pekerjaan { get; set; }
        public string Kewarganegaraan { get; set; }
        public DateTime Masa_berlaku { get; set; }
    }
}

